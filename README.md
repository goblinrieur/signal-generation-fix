# re-design a full fixed PCB 

the goal here is to have all-in-one PCB with all the fix proposed from

electro bidouilleur video _Améliorations au Générateur à Bas Coût Geekcreit XR2206_ [XR2206_fix_kit](https://www.youtube.com/watch?v=0ANx_F6nvWs)

electro bidouilleur video _Assemblage Générateur BF_ [XR2206_kit](https://www.youtube.com/watch?v=cPGbltBZa0o)

[electro-bidouilleur site](http://bidouilleur.ca/)

# Done

[PCB branch](https://gitlab.com/goblinrieur/signal-generation-fix/-/commits/PCB)

- [x] start project in kicad _v5.1.9_ updated to kicad _v8.1.0_

- [x] make hierarchic pages

- [X] get schematic as is 

- [X] apply changes to schematics

- [X] get annotations

- [X] chose components

- [X] list footprints

- [X] make net-list

- [X] check size of the box, the PCB must feat the box size

- [X] design PCB

- [X] build gerber files & drill files

- [X] update all data with KICAD 6.x

- [X] update all data with KICAD 8.x

- [X] purchase PCB

- [X] receive PCB

- [X] solder it

- [X] test it

- [X] insert in final box.

# Pics

![1](./1.png)

![2](./2.png)

![3](./3.png)

![4](./4.png)

![5](./face.png)

![6](./pcb.png)

