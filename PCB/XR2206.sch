EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "xr2206"
Date "2021-05-21"
Rev "0.0"
Comp ""
Comment1 "signal generator "
Comment2 "based on xr2206"
Comment3 "fix proposed by electrobidouilleur"
Comment4 "https://www.youtube.com/watch?v=0ANx_F6nvWs"
$EndDescr
$Sheet
S 1500 1700 3100 2300
U 60A7BF69
F0 "mainboard" 50
F1 "mainboardwithfixes.sch" 50
$EndSheet
$Sheet
S 5000 1700 3100 2300
U 60A7C025
F0 "add-ons" 50
F1 "add-ons.sch" 50
$EndSheet
$Sheet
S 1500 4500 3100 2400
U 60A7C09D
F0 "power" 50
F1 "power.sch" 50
$EndSheet
Text Notes 5400 2850 0    50   ~ 0
several compromises depending on use or type of load\n\nIf needed you can use a separated board for that part
Text Notes 2550 5700 0    50   ~ 0
Optionnal power input 
Text Notes 2000 2950 0    50   ~ 0
Based on XR2206CP I.C. & electrobidouilleur Idea's\n\nsee the main README.md on that project's GITLAB page\n\nSee Video in comments
Text Notes 5400 3250 0    50   Italic 10
All capacitors should be 16v compliant minimum.
Text Notes 2000 3250 0    50   Italic 10
All capacitors should be 16v compliant minimum.
Text Notes 5600 5450 0    50   Italic 10
I will choose to use all capacitors in SMD 0603 form factor for add-ons & mainboard.\n\nI will choose to use all resistors in SMD 0805 form factor for add-ons & mainboard.\n\nI will use standard sized componants for XR2206CP itself (dip16) & for all in the power parts.
$EndSCHEMATC
